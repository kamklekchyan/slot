﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Bet5 = new System.Windows.Forms.Button();
            this.Bet10 = new System.Windows.Forms.Button();
            this.Bet15 = new System.Windows.Forms.Button();
            this.Ballance = new System.Windows.Forms.TextBox();
            this.Winbox = new System.Windows.Forms.TextBox();
            this.labelLastWin = new System.Windows.Forms.Label();
            this.textBoxFirst = new System.Windows.Forms.TextBox();
            this.textBoxThird = new System.Windows.Forms.TextBox();
            this.textBoxSecond = new System.Windows.Forms.TextBox();
            this.labelBalance = new System.Windows.Forms.Label();
            this.ChanceBox = new System.Windows.Forms.TextBox();
            this.Chances = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Bet5
            // 
            this.Bet5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Bet5.Location = new System.Drawing.Point(89, 326);
            this.Bet5.Name = "Bet5";
            this.Bet5.Size = new System.Drawing.Size(100, 50);
            this.Bet5.TabIndex = 0;
            this.Bet5.Text = "5";
            this.Bet5.UseVisualStyleBackColor = true;
            this.Bet5.Click += new System.EventHandler(this.Bet5_Click);
            // 
            // Bet10
            // 
            this.Bet10.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Bet10.Location = new System.Drawing.Point(231, 326);
            this.Bet10.Name = "Bet10";
            this.Bet10.Size = new System.Drawing.Size(100, 50);
            this.Bet10.TabIndex = 1;
            this.Bet10.Text = "10";
            this.Bet10.UseVisualStyleBackColor = true;
            this.Bet10.Click += new System.EventHandler(this.Bet10_Click);
            // 
            // Bet15
            // 
            this.Bet15.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Bet15.Location = new System.Drawing.Point(379, 327);
            this.Bet15.Name = "Bet15";
            this.Bet15.Size = new System.Drawing.Size(100, 50);
            this.Bet15.TabIndex = 2;
            this.Bet15.Text = "15";
            this.Bet15.UseVisualStyleBackColor = true;
            this.Bet15.Click += new System.EventHandler(this.Bet15_Click);
            // 
            // Ballance
            // 
            this.Ballance.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Ballance.Location = new System.Drawing.Point(654, 41);
            this.Ballance.Multiline = true;
            this.Ballance.Name = "Ballance";
            this.Ballance.Size = new System.Drawing.Size(120, 49);
            this.Ballance.TabIndex = 3;
            this.Ballance.Text = Ballance.Text;
            this.Ballance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Ballance.TextChanged += new System.EventHandler(this.Ballance_TextChanged);
            // 
            // Winbox
            // 
            this.Winbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Winbox.Location = new System.Drawing.Point(657, 327);
            this.Winbox.Multiline = true;
            this.Winbox.Name = "Winbox";
            this.Winbox.Size = new System.Drawing.Size(117, 50);
            this.Winbox.TabIndex = 5;
            this.Winbox.Text = "0";
            this.Winbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Winbox.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // labelLastWin
            // 
            this.labelLastWin.AutoSize = true;
            this.labelLastWin.Location = new System.Drawing.Point(682, 311);
            this.labelLastWin.Name = "labelLastWin";
            this.labelLastWin.Size = new System.Drawing.Size(49, 13);
            this.labelLastWin.TabIndex = 6;
            this.labelLastWin.Text = "Last Win";
            // 
            // textBoxFirst
            // 
            this.textBoxFirst.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxFirst.Location = new System.Drawing.Point(89, 104);
            this.textBoxFirst.Multiline = true;
            this.textBoxFirst.Name = "textBoxFirst";
            this.textBoxFirst.Size = new System.Drawing.Size(140, 180);
            this.textBoxFirst.TabIndex = 7;
            this.textBoxFirst.Text = "0";
            this.textBoxFirst.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxFirst.TextChanged += new System.EventHandler(this.textBoxFirst_TextChanged);
            // 
            // textBoxThird
            // 
            this.textBoxThird.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxThird.Location = new System.Drawing.Point(367, 104);
            this.textBoxThird.Multiline = true;
            this.textBoxThird.Name = "textBoxThird";
            this.textBoxThird.Size = new System.Drawing.Size(140, 180);
            this.textBoxThird.TabIndex = 8;
            this.textBoxThird.Text = "0";
            this.textBoxThird.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxThird.TextChanged += new System.EventHandler(this.textBoxThird_TextChanged);
            // 
            // textBoxSecond
            // 
            this.textBoxSecond.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxSecond.Location = new System.Drawing.Point(222, 104);
            this.textBoxSecond.Multiline = true;
            this.textBoxSecond.Name = "textBoxSecond";
            this.textBoxSecond.Size = new System.Drawing.Size(150, 180);
            this.textBoxSecond.TabIndex = 9;
            this.textBoxSecond.Text = "0";
            this.textBoxSecond.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxSecond.TextChanged += new System.EventHandler(this.textBoxSecond_TextChanged);
            // 
            // labelBalance
            // 
            this.labelBalance.AutoSize = true;
            this.labelBalance.Location = new System.Drawing.Point(682, 25);
            this.labelBalance.Name = "labelBalance";
            this.labelBalance.Size = new System.Drawing.Size(46, 13);
            this.labelBalance.TabIndex = 4;
            this.labelBalance.Text = "Balance";
            // 
            // ChanceBox
            // 
            this.ChanceBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChanceBox.Location = new System.Drawing.Point(654, 189);
            this.ChanceBox.Multiline = true;
            this.ChanceBox.Name = "ChanceBox";
            this.ChanceBox.Size = new System.Drawing.Size(120, 49);
            this.ChanceBox.TabIndex = 10;
            this.ChanceBox.Text = ChanceBox.Text;
            this.ChanceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ChanceBox.TextChanged += new System.EventHandler(this.ChanceBox_TextChanged);
            // 
            // Chances
            // 
            this.Chances.AutoSize = true;
            this.Chances.Location = new System.Drawing.Point(685, 173);
            this.Chances.Name = "Chances";
            this.Chances.Size = new System.Drawing.Size(49, 13);
            this.Chances.TabIndex = 11;
            this.Chances.Text = "Chances";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Chances);
            this.Controls.Add(this.ChanceBox);
            this.Controls.Add(this.textBoxSecond);
            this.Controls.Add(this.textBoxThird);
            this.Controls.Add(this.textBoxFirst);
            this.Controls.Add(this.labelLastWin);
            this.Controls.Add(this.Winbox);
            this.Controls.Add(this.labelBalance);
            this.Controls.Add(this.Ballance);
            this.Controls.Add(this.Bet15);
            this.Controls.Add(this.Bet10);
            this.Controls.Add(this.Bet5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Bet5;
        private System.Windows.Forms.Button Bet10;
        private System.Windows.Forms.Button Bet15;
        private System.Windows.Forms.TextBox Ballance;
        private System.Windows.Forms.TextBox Winbox;
        private System.Windows.Forms.Label labelLastWin;
        private System.Windows.Forms.TextBox textBoxFirst;
        private System.Windows.Forms.TextBox textBoxThird;
        private System.Windows.Forms.TextBox textBoxSecond;
        private System.Windows.Forms.Label labelBalance;
        private System.Windows.Forms.TextBox ChanceBox;
        private System.Windows.Forms.Label Chances;
    }
}

