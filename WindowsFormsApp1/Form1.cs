﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form, IBet
    {

        Random rnd = new Random();
        int balance;
        int bet = 0;
        int chance;
        public Form1(int balance, int chance)
        {
            InitializeComponent();
            this.balance = balance;
            this.chance = chance;
            Ballance.Text = balance.ToString();
            ChanceBox.Text = chance.ToString();
        }
        public int GetBalance(int bet)
        {
            for (int i = 5; i <= 15; i += 5)
            {
                if (bet == i)
                {
                    balance -= i;
                    chance--;
                    break;
                }
            }
            return balance;
        }
        public void ClickBet(int bet)
        {
            Ballance.Text = GetBalance(bet).ToString();
            ChanceBox.Text = chance.ToString();
            textBoxFirst.Text = rnd.Next(0, 10).ToString();
            textBoxSecond.Text = rnd.Next(0, 10).ToString();
            textBoxThird.Text = rnd.Next(0, 10).ToString();
            if (textBoxFirst.Text == textBoxSecond.Text && textBoxSecond.Text == textBoxThird.Text)
            {
                balance += bet * int.Parse(textBoxFirst.Text);
                Ballance.Text = balance.ToString();
                Winbox.Text = (bet * int.Parse(textBoxFirst.Text)).ToString();
            }
            if (balance < 15)
            {
                Bet15.Enabled = false;
                if (balance < 10)
                {
                    Bet10.Enabled = false;
                }
            }
            if (chance == 0 || balance == 0)
            {
                MessageBox.Show("You Lose");
                Bet5.Enabled = false;
                Bet15.Enabled = false;
                Bet10.Enabled = false;
            }
        }

        private void Bet5_Click(object sender, EventArgs e)
        {
            bet = 5;
            ClickBet(bet);
        }

        private void Bet10_Click(object sender, EventArgs e)
        {
            bet = 10;
            ClickBet(bet);
        }

        private void Bet15_Click(object sender, EventArgs e)
        {
            bet = 15;
            ClickBet(bet);
        }

        private void textBoxFirst_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxSecond_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxThird_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void ChanceBox_TextChanged(object sender, EventArgs e)
        {
           
        }
        private void Ballance_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
